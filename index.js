const express = require('express')

//create an application using express
//this creates an express application and stores this as a constant called app.
//app is our server

const app = express()
//for our application server to run, we need a port to listen to
const port = 3000;

//setup for allowing the server to handle data from request
//allows your app to read json data
//middleware is software that provides service outside of what's offered by
//the operating system

app.use(express.json());
//allows your app to read data from forms
/* By default, information received from the url can
only be received as a sting or an array*/
/* By applying the option of "extended:true" tihs allows us to
receive information in other data types such as object/boolean
etc, which we will use throughout our application */
app.use (express.urlencoded({ extended: true}))

//we will create routes
//express has methods corresponding to each http method
//this routes expects to receive a get request at the base URI '/'
app.get('/', (req,res) => {
	//res.send uses the express js module method instead to send a response
	//back to the client.
	res.send("Hello World")
})

app.get('/hello', (req,res) =>{
	res.send('Hello from the /hello endpoint!')
})


app.post('/hello', (req,res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
})

//for sign up
// mock db

let users = [

];

app.post('/signup', (req,res) => {
	console.log(req.body)
	//if contents of the request body with the "username" and "password"
	// is not empty.
	if(req.body.username !== '' && req.body.password !== ''){
		//this will store the user object sent via postman to the users
		//array created above
		users.push(req.body)
		//send response
		res.send(`user ${req.body.username} successfully registered!`)
	} else{
		res.send("PLEASE INPUT BOTH USERNAME AND PASSWORD")
	}
})

/*app.put('/change-password', (req, res) => {
	let message;
	for(let i = 0; i < users.length; i++){
		if(req.body.username == users[i].username){

		users[i] = req.body.password

		message = `user ${req.body.username}'s password has been updated`
		break;
		}else{
			message = "user does not exist"
		}
	}
	
	res.send(message)
})*/

// 1. home 
app.get('/home', (req, res) =>{
	res.send("Welcome to the homepage!")
})
// 2. users retrieve
app.get('/users', (req, res) =>{
	res.send(users)
})
// 6. Delete
app.delete('/delete-users', (req, res) => {
	let message;
		for(let i = 0; i < users.length; i++){
        if(req.body.username == users[i].username){

            users[i].username = req.body.username
            users.splice(i,1)


            message = `User ${req.body.username} has been deleted`
            break;

        }else{
            message  = "user does not exist"
        }
    
    }
    res.send(message)
})
	

app.listen(port, () => console.log(`server is running at ${port}`))